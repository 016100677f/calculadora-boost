<?php require('components/head.inc.php'); ?>
<?php include('components/navbar.inc.php'); ?>
<section class="services gradient">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 220">
        <path
          fill="#fff"
          fill-opacity="1"
          d="M0,96L34.3,106.7C68.6,117,137,139,206,122.7C274.3,107,343,53,411,53.3C480,53,549,107,617,117.3C685.7,128,754,96,823,96C891.4,96,960,128,1029,154.7C1097.1,181,1166,203,1234,202.7C1302.9,203,1371,181,1406,170.7L1440,160L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
        ></path>
      </svg>
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-5">
            </button>

            <h1>CALCULADORA POTENTE </h1>

            <?php
   session_start();
   require_once 'Session.php';
   $n1 = Session::getAttribute2("num1");
   $n2 = Session::getAttribute2("num2");
   $rpta = Session::getAttribute2("rpta");
   $action = Session::getAttribute2("action");
?>
      <form method="post" action="index2.php">
         <div class="form-group">
            <label>Operacion</label>
            <select name="action" class="form-control">
               <option value="sumar">Suma</option>
               <option value="restar">Resta</option>
               <option value="multiplicar">Producto</option>
               <option value="dividir">Division</option>
               <option value="factorial">Factorial</option>
               <option value="potencia">Potencia</option>
               <option value="seno">Seno</option>
               <option value="tangente">Tangente</option>
               <option value="porcentaje">Porcentaje</option>
               <option value="raiz">Raiz</option>
               <option value="inversa">Inversa</option>
            </select>
         </div>
         <div class="form-group">
            <label>Número 1</label>
            <input type="text"  class="form-control" name="num1">
         </div>
         <div class="form-group">
            <label>Número 2</label>
            <input type="text" class="form-control" name="num2">
         </div>
         <div>
            <input type="submit" class="btn btn-danger" value="Procesar">
         </div>
      </form>
      <div>
         <table>
            <tr>
               <td>Operacion:</td>
               <td><?php echo($action); ?></td>
            </tr>
            <tr>
               <td>Num 1 :</td>
               <td><?php echo($n1); ?></td>
            </tr>
            <tr>
               <td>Num 2 :</td>
               <td><?php echo($n2); ?></td>
            </tr>
            <tr>
               <td>Rpta :</td>
               <td><?php echo($rpta); ?></td>
            </tr>
         </table>
      </div>
          </div>
          <div class="col-md-5"><img src="img/revenue_.svg" alt="" /></div>
        </div>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 210">
        <path
          fill="#fff"
          fill-opacity="1"
          d="M0,96L34.3,106.7C68.6,117,137,139,206,122.7C274.3,107,343,53,411,53.3C480,53,549,107,617,117.3C685.7,128,754,96,823,96C891.4,96,960,128,1029,154.7C1097.1,181,1166,203,1234,202.7C1302.9,203,1371,181,1406,170.7L1440,160L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z"
        ></path>
      </svg>
    </section>
<?php include('components/companies.inc.php'); ?>
<?php require('components/footer.inc.php'); ?>
