<?php
session_start();
require_once 'matematica.php';
require_once 'session.php';
$action = $_REQUEST["action"];

$obj = new Controller();
$target = call_user_func(array($obj,$action));
header("location: $target");
return;

class Controller {
   public function sumar() {
      $n1 = $_REQUEST["num1"];
      $n2 = $_REQUEST["num2"];
      $mate = new Matematica();
      $rpta = $mate->suma($n1, $n2);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("num2", $n2);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "Sumar");
      return "services.php";
   }

   public function restar() {
      $n1 = $_REQUEST["num1"];
      $n2 = $_REQUEST["num2"];
      $mate = new Matematica();
      $rpta = $mate->resta($n1, $n2);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("num2", $n2);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "Restar");
      return "services.php";
   }

   public function multiplicar() {
      $n1 = $_REQUEST["num1"];
      $n2 = $_REQUEST["num2"];
      $mate = new Matematica();
      $rpta = $mate->producto($n1, $n2);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("num2", $n2);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "Multiplicar");
      return "services.php";
   }

   public function dividir() {
      $n1 = $_REQUEST["num1"];
      $n2 = $_REQUEST["num2"];
      $mate = new Matematica();
      $rpta = $mate->divide($n1, $n2);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("num2", $n2);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "Dividir");
      return "services.php";
   }

   public function factorial() {
      $n1 = $_REQUEST["num1"];
      $mate = new Matematica();
      $rpta = $mate->factorial($n1);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "factorial");
      return "services.php";
   }

   public function potencia() {
      $n1 = $_REQUEST["num1"];
      $n2 = $_REQUEST["num2"];
      $mate = new Matematica();
      $rpta = $mate->potencia($n1, $n2);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("num2", $n2);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "potencia");
      return "services.php";
   }
   
   public function seno() {
      $n1 = $_REQUEST["num1"];
      $mate = new Matematica();
      $rpta = $mate->seno($n1);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "Seno");
      return "services.php";
   }
   
   public function tangente() {
      $n1 = $_REQUEST["num1"];
      $mate = new Matematica();
      $rpta = $mate->tangente($n1);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "Tangente");
      return "services.php";
   }
   public function porcentaje() {
      $n1 = $_REQUEST["num1"];
      $n2 = $_REQUEST["num2"];
      $mate = new Matematica();
      $rpta = $mate->porcentaje($n1, $n2);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("num2", $n2);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "porcentaje");
      return "services.php";
   }
   public function raiz() {
      $n1 = $_REQUEST["num1"];
      $mate = new Matematica();
      $rpta = $mate->raiz($n1);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "raiz");
      return "services.php";
   }
   public function inversa() {
      $n1 = $_REQUEST["num1"];
      $mate = new Matematica();
      $rpta = $mate->inversa($n1);
      Session::setAttribute("num1", $n1);
      Session::setAttribute("rpta", $rpta);
      Session::setAttribute("action", "inversa");
      return "services.php";
   }
}
